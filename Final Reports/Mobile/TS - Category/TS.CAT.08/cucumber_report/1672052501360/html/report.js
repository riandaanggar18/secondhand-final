$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Category.feature");
formatter.feature({
  "name": "Category",
  "description": "\tSebagai User, saya ingin melakukan pengelompokkan barang berdasarkan kategori pada aplikasi Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan sortir produk dengan memilih kategori yang tersedia",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Beranda",
  "keyword": "When "
});
formatter.match({
  "location": "Category.user_klik_button_Beranda()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Di delay 20 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.di_delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 5.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Komputer dan Aksesoris",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Komputer_dan_Aksesoris()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 5.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Komputer dan Aksesoris",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Komputer_dan_Aksesoris()"
});
formatter.result({
  "status": "passed"
});
});