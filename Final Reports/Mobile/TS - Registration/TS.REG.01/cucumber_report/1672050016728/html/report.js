$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Registration.feature");
formatter.feature({
  "name": "Registration",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun tanpa mengisi semua input field yang dibutuhkan",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik text button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_text_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa field tidak boleh kosong",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mendapatkan_alert_bahwa_field_tidak_boleh_kosong()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun menggunakan email yang tidak valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email dengan tidak valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_dengan_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nomor hp dengan \"085445695220\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nomor_hp_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field kota dengan \"Bandung\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_kota_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field alamat dengan \"Jalan kaki\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_alamat_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User scroll layar ke button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_scroll_layar_ke_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa email tidak valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mendapatkan_alert_bahwa_email_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun menggunakan password yang tidak valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field nama Pukaxa",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_nama_Pukaxa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field email pukaxa521",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_email_pukaxa521()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password RigbBhfdqOBGNlJIWM1ClA",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_password_RigbBhfdqOBGNlJIWM1ClA()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email dengan email baru",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_dengan_email_baru()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan tidak valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan alert bahwa password minimal 6 karakter",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mendapatkan_alert_bahwa_password_minimal_karakter(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun menggunakan email yang sudah terdaftar",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field nama Pukaxa",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_nama_Pukaxa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field email sebelumnya",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_email_sebelumnya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password tzH6RvlfSTg",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_password_tzH6RvlfSTg()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email yang sudah terdaftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_yang_sudah_terdaftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun dengan mengisi semua input field yang dibutuhkan dengan valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field nama Pukaxa",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_nama_Pukaxa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field email pukaxa721",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_email_pukaxa721()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email dengan valid - random \"aaaa@getnada.com\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_dengan_valid_random(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User berhasil masuk ke halaman Akun Saya",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_berhasil_masuk_ke_halaman_Akun_Saya()"
});
formatter.result({
  "status": "passed"
});
});