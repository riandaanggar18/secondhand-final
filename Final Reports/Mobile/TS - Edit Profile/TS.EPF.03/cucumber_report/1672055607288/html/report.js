$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Profile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User ingin meng-edit profil",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Akun",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik tanda pencil",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.klik_tanda_pencil()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Upload Foto Dari Galeri",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.upload_Foto_Dari_Galeri()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify Upload foto",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.verify_Upload_foto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ubah nama dengan nama yang valid",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.ubah_nama_dengan_nama_yang_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify ubah nama",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.verify_ubah_nama()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ubah no handphone dengan no handphone yang tidak valid",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.ubah_no_handphone_dengan_no_handphone_yang_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify ubah no handphone",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.verify_ubah_no_handphone()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ubah password dengan input semua field dengan kata sandi lama",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.ubah_password_dengan_input_semua_field_dengan_kata_sandi_lama()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify ubah password",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.verify_ubah_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 5.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik panah kembali",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.klik_panah_kembali()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik Beranda",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.klik_Beranda()"
});
formatter.result({
  "status": "passed"
});
});