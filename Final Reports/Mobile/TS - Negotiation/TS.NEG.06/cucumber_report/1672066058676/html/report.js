$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/platinum-challenge-kelompok-3-final/Secondhand-App/Include/features/Buyer.feature");
formatter.feature({
  "name": "buyer Nego",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "buyer nego with valid data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Klik tombol kolom cari produk",
  "keyword": "And "
});
formatter.match({
  "location": "Buyer.klik_tombol_kolom_cari_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user ketik produk",
  "keyword": "Then "
});
formatter.match({
  "location": "Buyer.user_ketik_produk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "delay",
  "keyword": "Then "
});
formatter.match({
  "location": "Buyer.delay()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik produk kabel lan",
  "keyword": "Then "
});
formatter.match({
  "location": "Buyer.klik_produk_kabel_lan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik nego kabel lan",
  "keyword": "And "
});
formatter.match({
  "location": "Buyer.klik_nego_kabel_lan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "input harga nego kabel lan",
  "keyword": "Then "
});
formatter.match({
  "location": "Buyer.input_harga_nego_kabel_lan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik tombol kirim",
  "keyword": "And "
});
formatter.match({
  "location": "Buyer.klik_tombol_kirim()"
});
formatter.result({
  "status": "passed"
});
});