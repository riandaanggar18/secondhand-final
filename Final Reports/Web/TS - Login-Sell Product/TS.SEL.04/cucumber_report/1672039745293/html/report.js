$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/SellProduct.feature");
formatter.feature({
  "name": "SellProduct",
  "description": "\tSebagai User, saya ingin menambah produk jualan saya pada web Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan tambah produk dengan mengisi semua input field yang dibutuhkan dengan valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button + Jual",
  "keyword": "When "
});
formatter.match({
  "location": "SellProduct.user_klik_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama produk dengan \"Patung Motor\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_nama_produk_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field harga produk dengan \"15000\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_harga_produk_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User memilih kategori Hoby",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_memilih_kategori_Hoby()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field deskripsi",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_mengisi_field_deskripsi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "SellProduct.user_klik_button_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik tombol list",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduk.klik_tombol_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});