$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Profile.feature");
formatter.feature({
  "name": "Profile",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Pengguna ingin dapat berhasil mengubah data profile",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Klik tombol user",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.klik_tombol_user()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik tombol Profile",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.klik_tombol_Profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melakukan submit tanpa mengisi atau merubah value pada semua field",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.user_melakukan_submit_tanpa_mengisi_atau_merubah_value_pada_semua_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik submit lagi ya",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.klik_submit_lagi_ya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify berhasil",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.verify_berhasil()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melakukan ubah nama menggunakan nama baru yang tidak valid",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.user_melakukan_ubah_nama_menggunakan_nama_baru_yang_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik submit lagi juga",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.klik_submit_lagi_juga()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify berhasil ya",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.verify_berhasil_ya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melakukan ubah nomor hp menggunakan nomor hp baru yang tidak valid",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.user_melakukan_ubah_nomor_hp_menggunakan_nomor_hp_baru_yang_tidak_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik submit lagi",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.klik_submit_lagi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify berhasil lagi",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.verify_berhasil_lagi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengisi semua input field yang dibutuhkan dengan valid",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.mengisi_semua_input_field_yang_dibutuhkan_dengan_valid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik submit juga lagi",
  "keyword": "Then "
});
formatter.match({
  "location": "Profile.klik_submit_juga_lagi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify berhasil juga",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.verify_berhasil_juga()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "kembali ke Home page",
  "keyword": "And "
});
formatter.match({
  "location": "Profile.kembali_ke_Home_page()"
});
formatter.result({
  "status": "passed"
});
});