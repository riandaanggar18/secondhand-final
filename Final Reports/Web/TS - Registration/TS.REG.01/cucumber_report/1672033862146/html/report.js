$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Registration.feature");
formatter.feature({
  "name": "Registration",
  "description": "\tSebagai User, saya ingin melakukan registrasi pada web Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun tanpa mengisi semua input field yang dibutuhkan",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Masuk",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik text button Daftar di sini",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_text_button_Daftar_di_sini()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun menggunakan email yang tidak valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email dengan \"pukaxa500@\" untuk email yang tidak valid",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_dengan_untuk_email_yang_tidak_valid(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan \"RigbBhfdqOBGNlJIWM1ClA\u003d\u003d\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun menggunakan email yang sudah terdaftar",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field nama",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_nama()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field email",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email dengan \"pukaxa500@getnada.com\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan \"RigbBhfdqOBGNlJIWM1ClA\u003d\u003d\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan info email sudah digunakan",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mendapatkan_info_email_sudah_digunakan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User melakukan pendaftaran akun dengan mengisi semua input field yang dibutuhkan dengan valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Mengosongkan field nama",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_nama()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field email",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mengosongkan field password",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.mengosongkan_field_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field nama dengan \"Pukaxa\"",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_mengisi_field_nama_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email \"pukaahsb@getnada.com\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password dengan \"RigbBhfdqOBGNlJIWM1ClA\u003d\u003d\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mengisi_field_password_dengan(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik button Daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_klik_button_Daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mendapatkan info untuk verifikasi email",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_mendapatkan_info_untuk_verifikasi_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});