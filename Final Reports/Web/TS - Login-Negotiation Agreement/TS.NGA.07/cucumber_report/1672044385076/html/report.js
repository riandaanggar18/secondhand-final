$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Seller.feature");
formatter.feature({
  "name": "Seller Nego Agreement",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Tolak dan terima nego",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "klik list sale",
  "keyword": "Then "
});
formatter.match({
  "location": "Seller.klik_list_sale()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik tombol diminati",
  "keyword": "And "
});
formatter.match({
  "location": "Seller.klik_tombol_diminati()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "pilih product yang diminati",
  "keyword": "Then "
});
formatter.match({
  "location": "Seller.pilih_product_yang_diminati()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Tekan tombol terima",
  "keyword": "And "
});
formatter.match({
  "location": "Seller.tekan_tombol_terima()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});