$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Logout.feature");
formatter.feature({
  "name": "Logout",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "user ingin keluar dari website",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Klik tombol user",
  "keyword": "When "
});
formatter.match({
  "location": "Profile.klik_tombol_user()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Klik button logout",
  "keyword": "And "
});
formatter.match({
  "location": "Logout.klik_button_logout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify logout",
  "keyword": "And "
});
formatter.match({
  "location": "Logout.verify_logout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});