$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Valid/Login-BuyerPositiveOnly.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan login akun dengan menggunakan email dan password yang valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User klik button Masuk",
  "keyword": "When "
});
formatter.match({
  "location": "Registration.user_klik_button_Masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field email login dengan akun buyer",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_mengisi_field_email_login_dengan_akun_buyer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User mengisi field password login \"123456789\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_mengisi_field_password_login(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik tombol masuk",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_klik_tombol_masuk()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 3 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Registration.delay_detik(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});