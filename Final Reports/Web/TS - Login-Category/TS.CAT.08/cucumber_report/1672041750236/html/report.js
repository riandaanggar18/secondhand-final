$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/nandi/Katalon Studio/PlatinumChallenge_Kelompok3/Secondhand-Web/Include/features/Category.feature");
formatter.feature({
  "name": "Category",
  "description": "\tSebagai User, saya ingin melakukan pengelompokkan barang berdasarkan kategori pada web Second Hand Store",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User melakukan sortir produk dengan memilih kategori yang tersedia",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User scroll ke Telusuri Kategori",
  "keyword": "When "
});
formatter.match({
  "location": "Category.user_scroll_ke_Telusuri_Kategori()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Hoby",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Hoby()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Hoby",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Hoby()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 1.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Elektronik",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Elektronik()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 1.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Kendaraan",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Kendaraan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Kendaraan",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Kendaraan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 1.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Baju",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Baju()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Baju",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Baju()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 1.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User klik kategori Kesehatan",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_klik_kategori_Kesehatan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User melihat barang-barang pada kategori Kesehatan",
  "keyword": "Then "
});
formatter.match({
  "location": "Category.user_melihat_barang_barang_pada_kategori_Kesehatan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Delay 1.5 detik",
  "keyword": "Given "
});
formatter.match({
  "location": "Category.delay_detik(Double)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User screenshot layar",
  "keyword": "Then "
});
formatter.match({
  "location": "Registration.user_screenshot_layar()"
});
formatter.result({
  "status": "passed"
});
});