<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_closeberhasil</name>
   <tag></tag>
   <elementGuidId>c3a76253-98b9-4739-8211-7534f0155cfb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'svg-inline--fa fa-xmark ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-xmark </value>
      <webElementGuid>d18973f9-09c0-4e93-a386-91a0c3c666af</webElementGuid>
   </webElementProperties>
</WebElementEntity>
