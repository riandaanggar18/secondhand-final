<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_list_sale</name>
   <tag></tag>
   <elementGuidId>22b50896-42a8-4cef-a931-0395ad56dac7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-icon = 'list']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>ba44823f-6a47-498e-8ea8-65ed13ac8cfc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
