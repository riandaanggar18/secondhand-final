Feature: Login
	User ingin login pada website SecondHand
	
	Scenario: User melakukan login akun tanpa mengisi semua input field yang dibutuhkan
	When User klik button Masuk
	Given Delay 3 detik
	Then User klik tombol masuk
	Given Delay 3 detik
	Then User screenshot layar
	
	Scenario: User melakukan login akun tanpa mengisi input field password
	When User mengisi field email login dengan valid
	Then User klik tombol masuk
	Given Delay 3 detik
	Then User screenshot layar
	
	Scenario: User melakukan login akun menggunakan email yang tidak terdaftar/salah
	Given Menghapus field password login
	When User mengisi field email login yang salah
	Then User mengisi field password login "123456789"
	Then User klik tombol masuk
	Given Delay 3 detik
	Then User mendapatkan error message
	Then User screenshot layar
	
	Scenario: User melakukan login akun menggunakan email yang belum diverifikasi/aktivasi
	Given Menghapus field email login
	Given Menghapus field password login
	When User mengisi field email login yang belom di verifikasi
	Then User mengisi field password email yang belom di verif "12345678"
	Then User klik tombol masuk
	Given Delay 3 detik
	Then User mendapatkan error message verif
	Then User screenshot layar
	
	Scenario: User melakukan login akun dengan menggunakan email dan password yang valid
	Given User menghapus field email login
	Given User menghapus field password login
	When User mengisi field email login dengan valid
	Then User mengisi field password login "123456789"
	Then User klik tombol masuk
	Given Delay 3 detik
	Then User screenshot layar