Feature: Registration
	Sebagai User, saya ingin melakukan registrasi pada web Second Hand Store

	Scenario: User melakukan pendaftaran akun tanpa mengisi semua input field yang dibutuhkan
		When User klik button Masuk
		Given Delay 3 detik
		Then User klik text button Daftar di sini
		Then User klik button Daftar
		Given Delay 3 detik
		Then User screenshot layar
		
	Scenario: User melakukan pendaftaran akun menggunakan email yang tidak valid
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email dengan "pukaxa500@" untuk email yang tidak valid
		Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
		Then User klik button Daftar
		Given Delay 3 detik
		Then User screenshot layar
		
	Scenario: User melakukan pendaftaran akun menggunakan email yang sudah terdaftar
		Given Mengosongkan field nama
		Given Mengosongkan field email
		Given Mengosongkan field password
		When User mengisi field nama dengan "Pukaxa"
		Then User mengisi field email dengan "pukaxa500@getnada.com"
		Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
		Then User klik button Daftar
		Given Delay 3 detik
		Then User mendapatkan info email sudah digunakan
		Then User screenshot layar
	
	Scenario: User melakukan pendaftaran akun dengan mengisi semua input field yang dibutuhkan dengan valid
    Given Mengosongkan field nama
    Given Mengosongkan field email
    Given Mengosongkan field password
    When User mengisi field nama dengan "Pukaxa"
    Then User mengisi field email "pukaahsb@getnada.com"
    Then User mengisi field password dengan "RigbBhfdqOBGNlJIWM1ClA=="
    Then User klik button Daftar
    Given Delay 3 detik
    Then User mendapatkan info untuk verifikasi email
    Then User screenshot layar
