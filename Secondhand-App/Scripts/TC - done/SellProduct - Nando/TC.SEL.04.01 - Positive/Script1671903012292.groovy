import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/btn_Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/btn_Plus'), 0)

Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_NamaProduk'), 'Patung Motor', 0)

Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_HargaProduk'), '150000', 0)

Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/field_PilihKategori'), 0)

Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/elektronik'), 0)

Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_LokasiProduk'), 'Jakarta', 0)

Mobile.setText(findTestObject('Object Repository/Page_TambahProduk/inputfield_DeskripsiProduk'), 'Ini adalah patung motor', 
    0)

Mobile.tap(findTestObject('Page_TambahProduk/btn_UploadFotoProduk'), 0)

Mobile.tap(findTestObject('Object Repository/Page_TambahProduk/btn_Gallery'), 0)

Mobile.tap(findTestObject('Page_TambahProduk/btn_Terbitkan'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Page_TambahProduk/verify_berhasilditerbitkan'), 0)

