package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Login {
	@Then("User klik button Masuk Login")
	public void user_klik_button_Masuk_Login() {
		Mobile.tap(findTestObject('Object Repository/Page_Login/btn_MasukLogin'), 0)
	}

	@Then("User mendapatkan alert bahwa field email login tidak boleh kosong")
	public void user_mendapatkan_alert_bahwa_field_email_login_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Login/verify_emailtidakbolehkosong'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@When("User mengisi field email login dengan email yang tidak terdaftar")
	public void user_mengisi_field_email_login_dengan_email_yang_tidak_terdaftar() {
		Mobile.setText(findTestObject('Object Repository/Page_Login/inputfield - Masukkan email'), 'kasodma@getnada.com', 0)
	}

	@Then("User mengisi field password login dengan valid")
	public void user_mengisi_field_password_login_dengan_valid() {
		Mobile.setEncryptedText(findTestObject('Page_Login/inputfield - Masukkan password'), 'RigbBhfdqOBGNlJIWM1ClA==', 0)
	}

	@Given("Mengosongkan field email yang tidak terdaftar")
	public void mengosongkan_field_email_yang_tidak_terdaftar() {
		Mobile.clearText(findTestObject('Object Repository/Page_Login/inputfield - kasodmagetnada.com'), 0)
	}

	@Given("Mengosongkan field password yang valid")
	public void mengosongkan_field_password_yang_valid() {
		Mobile.clearText(findTestObject('Object Repository/Page_Login/inputfield - 12345678'), 0)
	}

	@When("User mengisi field email login dengan email yang valid")
	public void user_mengisi_field_email_login_dengan_email_yang_valid() {
		Mobile.setText(findTestObject('Page_Login/inputfield - Masukkan email'), 'pukaxa721@getnada.com', 0)
	}

	@Then("User mengisi field password dengan password yang salah")
	public void user_mengisi_field_password_dengan_password_yang_salah() {
		Mobile.setEncryptedText(findTestObject('Page_Login/inputfield - Masukkan password'), 'M1Lnyl0phgn1JV2kuvT1pw==', 0)
	}

	@Given("Mengosongkan field email login sebelumnya")
	public void mengosongkan_field_email_login_sebelumnya() {
		Mobile.clearText(findTestObject('Object Repository/Page_Login/inputfield - pukaxa721getnada.com'), 0)
	}

	@Given("Mengosongkan field password login yang salah")
	public void mengosongkan_field_password_login_yang_salah() {
		Mobile.clearText(findTestObject('Object Repository/Page_Login/inputfield - 12341234'), 0)
	}

	@Then("User masuk ke halaman Akun Saya")
	public void user_masuk_ke_halaman_Akun_Saya() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Login/verify_akunsaya'), 0, FailureHandling.STOP_ON_FAILURE)
	}
}
