package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class EditProduct {
	@Then("User klik menu Daftar Jual Saya")
	public void user_klik_menu_Daftar_Jual_Saya() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/btn_DaftarJualSaya'), 0)
	}

	@Then("User klik salah satu produk")
	public void user_klik_salah_satu_produk() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/card_produk'), 0)
	}

	@Then("User mengosongkan field nama produk")
	public void user_mengosongkan_field_nama_produk() {
		Mobile.clearText(findTestObject('Object Repository/Edit Product/field_namaproduk - Motor Mainan'), 0)
	}

	@Then("User mendapatkan alert bahwa field nama produk tidak boleh kosong")
	public void user_mendapatkan_alert_bahwa_field_nama_produk_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Edit Product/verify_namaproduktidakbolehkosong'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User mengosongkan field harga produk")
	public void user_mengosongkan_field_harga_produk() {
		Mobile.clearText(findTestObject('Object Repository/Edit Product/field_hargaproduk - 150000'), 0)
	}

	@Then("User mendapatkan alert bahwa field harga produk tidak boleh kosong")
	public void user_mendapatkan_alert_bahwa_field_harga_produk_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Edit Product/verify_hargatidakbolehkosong'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User mengosongkan field lokasi produk")
	public void user_mengosongkan_field_lokasi_produk() {
		Mobile.clearText(findTestObject('Object Repository/Edit Product/field_lokasi - Jakarta'), 0)
	}

	@Then("User mendapatkan alert bahwa field lokasi produk tidak boleh kosong")
	public void user_mendapatkan_alert_bahwa_field_lokasi_produk_tidak_boleh_kosong() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Edit Product/verify_lokasitidakbolehkosong'), 0, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User back ke Akun saya")
	public void user_back_ke_Akun_saya() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/btn_backbackback'), 0)
	}

	@When("User klik salah satu produk pada daftar jual")
	public void user_klik_salah_satu_produk_pada_daftar_jual() {
		Mobile.tap(findTestObject('Edit Product/card_produk'), 0)
	}

	@Then("User mengisi field nama produk dengan nama produk baru")
	public void user_mengisi_field_nama_produk_dengan_nama_produk_baru() {
		Mobile.setText(findTestObject('Object Repository/Edit Product/inputfield_namaproduk - Nama Produk'), 'Motor Mainan 2', 0)
	}

	@Then("User klik button Perbarui Produk")
	public void user_klik_button_Perbarui_Produk() {
		Mobile.tap(findTestObject('Object Repository/Edit Product/btn_PerbaruiProduk'), 0)
	}
}
