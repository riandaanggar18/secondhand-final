Feature: Login

	Scenario: User melakukan login akun tanpa mengisi input field apapun
		When User klik button Akun
		Then User klik button Masuk
		Given Delay 3 detik
		Then User klik button Masuk Login
		Given Delay 3 detik
		Then User mendapatkan alert bahwa field email login tidak boleh kosong
		
	Scenario: User melakukan login akun menggunakan email yang tidak terdaftar/salah
		When User mengisi field email login dengan email yang tidak terdaftar
		Then User mengisi field password login dengan valid
		Then User klik button Masuk Login
		Given Delay 3 detik
		
	Scenario: User melakukan login akun menggunakan password yang tidak terdaftar/salah
		Given Mengosongkan field email yang tidak terdaftar
		Given Mengosongkan field password yang valid
		When User mengisi field email login dengan email yang valid
		Then User mengisi field password dengan password yang salah
		Then User klik button Masuk Login
		Given Delay 3 detik
		
	Scenario: User melakukan login akun menggunakan email dan password yang valid
		Given Mengosongkan field email login sebelumnya
		Given Mengosongkan field password login yang salah
		When User mengisi field email login dengan email yang valid
		Then User mengisi field password login dengan valid
		Then User klik button Masuk Login
		Given Delay 3 detik
		Then User masuk ke halaman Akun Saya
