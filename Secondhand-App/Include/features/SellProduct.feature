Feature: SellProduct
	Sebagai User, saya ingin menambah produk jualan saya pada aplikasi Second Hand Store

	Scenario: User melakukan tambah produk dengan mengisi semua input field yang dibutuhkan dengan valid
		When User klik button Akun
		Then User klik button +
		Then User mengisi field nama produk dengan "Patung Motor"
		Then User mengisi field harga produk dengan "150000"
		Then User klik field Pilih Kategori
		Then User memilih kategori Elektronik
		Then User mengisi field lokasi
		Then User mengisi field deskripsi
		Then User klik button upload foto produk
		Then User klik Galeri
		Then User memilih foto dari galeri
		Then User klik button Terbitkan
		Given Delay 3 detik
		Then User melihat informasi produk berhasil diterbitkan