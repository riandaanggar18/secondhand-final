Feature: Category
	Sebagai User, saya ingin melakukan pengelompokkan barang berdasarkan kategori pada aplikasi Second Hand Store

	Scenario: User melakukan sortir produk dengan memilih kategori yang tersedia
		When User klik button Akun
		When User klik button Beranda
		Given Di delay 20 detik
		Then User klik kategori Elektronik
		Given Delay 5.5 detik
		Then User melihat barang-barang pada kategori Elektronik
		Then User klik kategori Komputer dan Aksesoris
		Given Delay 5.5 detik
		Then User melihat barang-barang pada kategori Komputer dan Aksesoris